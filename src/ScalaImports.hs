{-# LANGUAGE OverloadedStrings #-}
module ScalaImports where


import CreateImport
import Data.Monoid
import qualified Data.ByteString.Char8 as B
import Data.ByteString.Char8 (ByteString)
import Data.Char (isUpper, isAlphaNum)
import Data.List (nub, find)
import qualified Data.HashTable.IO as H
import qualified Data.Set as S
import Data.List (foldl')

import Cache
import Types



defaultIgnore :: S.Set ByteString
defaultIgnore = S.fromList [ "Data"
                           , "IllegalArgumentException"
                           , "Result"
                           , "A"
                           , "Long"
                           , "Byte"
                           , "Error"
                           , "Array"
                           , "Item"
                           , "TR"
                           , "String"
                           , "Directory"
                           , "Double"
                           , "Class"
                           , "T"
                           , "Thread"
                           , "URL"
                           ]








instance Eq ScalaImport where
    (ScalaImport root1 _) == (ScalaImport root2 _) = root1 == root2

instance Ord ScalaImport where
    compare (ScalaImport root1 _) (ScalaImport root2 _) = compare root1 root2

instance Monoid ScalaImport where
    mempty = ScalaImport "" []
    mappend (ScalaImport val1 xs)  (ScalaImport val2 ys) = ScalaImport val1 (xs ++ ys)


instance Show ScalaImport where
    show (ScalaImport i xs) = "import _root_." ++ B.unpack i ++ mostrarModulos xs


mostrarModulos :: [ByteString] -> String
mostrarModulos []   = ""
mostrarModulos [x]  = "." ++ B.unpack x
mostrarModulos xs   = ".{ " ++ (fixComa (mostrarModulos' xs)) ++ " }"
    where
        mostrarModulos' :: [ByteString] -> String
        mostrarModulos' []      = ""
        mostrarModulos' (x:xs)  = B.unpack x ++ ", " ++ mostrarModulos' xs

        fixComa :: String -> String
        fixComa = reverse . drop 2 . reverse





obtenerModulos :: ByteString -> [ScalaImport]
obtenerModulos = map (\e -> ScalaImport (getRoot e) (getModulo e)) . soloImports . B.lines
    where
        soloImports :: [ByteString] -> [ByteString]
        soloImports []     = []
        soloImports (x:xs) = if length (B.splitWith (==' ') x) > 0
                                then if (B.splitWith (==' ') x) !! 0 == "import" && "_root_" `B.isPrefixOf` ((B.splitWith (==' ') x) !! 1)
                                        then  x : soloImports xs
                                        else soloImports xs
                                else soloImports xs


getRoot :: ByteString -> ByteString
getRoot = B.drop 1 
        . B.dropWhile (/='.')
        . B.reverse 
        . B.drop 1 
        . B.dropWhile (/='.') 
        . B.reverse

-- Obtiene los modulos de un string de import. Primero saca lo ultimo, lo que
-- esta despues del ultimo punto al dar vuelta el string. Despues se sacan las
-- llaves que puedan tener en el string debido a que podria ser un import
-- multiple. Finalmente se separa la linea usando las comas ',' porque puede ser
-- un import multiple
getModulo :: ByteString -> [ByteString]
getModulo = map (remove (==' '))
          . B.splitWith (==',') 
          . remove (=='}')
          . remove (=='{')
          . B.reverse 
          . B.takeWhile (/='.') 
          . B.reverse



remove :: (Char -> Bool) -> ByteString -> ByteString
remove f bs
    | bs == "" = ""
    | otherwise  = if f (B.head bs) then remove f (B.tail bs)
                    else (B.head bs) `B.cons` remove f (B.tail bs)



getIdentificadores :: S.Set ByteString -> ByteString -> [ByteString]
getIdentificadores ignore = filter (\b -> not (S.member b ignore))
                          . nub
                          . concat
                          . map findIdent
                          . removeComments
                          . B.lines

    where
        identificador :: ByteString -> Bool
        identificador = isUpper . B.head


        removeComments :: [ByteString] -> [ByteString]
        removeComments = removeLineComment

        removeLineComment :: [ByteString] -> [ByteString]
        removeLineComment []     = []
        removeLineComment (x:xs) = findLineComment x : removeLineComment xs


        findLineComment :: ByteString -> ByteString
        findLineComment bs
            | B.length bs > 2 = if (B.head bs) == '/' && (B.head . B.tail $ bs) == '/' 
                                    then ""
                                    else (B.head bs) `B.cons` findLineComment (B.tail bs)
            | otherwise       = bs
                                    





findIdent :: ByteString -> [ByteString]
findIdent = filter (isUpper . B.head) . filter (not . B.null) . B.splitWith fin
    where
        {-ident =  B.takeWhile fin . fst . B.unzip . dropWhile (\(p, n) -> not (fin p) && not (isUpper n)) . B.zip bs $ B.tail bs-}

        {-next =  B.dropWhile fin . fst . B.unzip . dropWhile (\(p, n) -> not (fin p) && not (isUpper n)) . B.zip bs $ B.tail bs-}

        {-ident = filter (isUpper . head) . B.splitWith fin $ bs-}

        fin c = c == '}' 
             || c == '{' 
             || c == '(' 
             || c == ')' 
             || c == ':'
             || c == '['
             || c == ']'
             || c == ','
             || c == ' '
             || c == '\"'


notImported :: [ScalaImport] -> [ByteString] -> [ByteString]
notImported ss xs = f (getAllImported ss) xs

    where
        f :: [ByteString] -> [ByteString] -> [ByteString]
        f _ []      = []
        f im (x:xs) = case find (==x) im of
                            Nothing -> x : f im xs
                            Just _  -> f im xs


getAllImported :: [ScalaImport] -> [ByteString]
getAllImported [] = []
getAllImported ((ScalaImport _ mods):xs) = mods ++ getAllImported xs



generateImports :: Cache -> [ByteString] -> IO [ScalaImport]
generateImports _  []        = return []
generateImports cache (x:xs) = do

    result <- H.lookup cache (B.unpack x)

    case result of
        Nothing -> generateImports cache xs
        Just im -> do

            ls <- generateImports cache xs
            return $ (ScalaImport (toModule im) [x]) : ls


    where
        
        toModule :: String -> ByteString
        toModule = removeJava . replaceBS f . B.pack 
            where
                f '/' = (True, '.')
                f _   = (False, ' ')

        removeJava :: ByteString -> ByteString
        removeJava = B.reverse . f . f . B.reverse

        f = B.drop 1 . B.dropWhile (/='.') 



mergeImports :: [ScalaImport] -> [ScalaImport] -> [ScalaImport]
mergeImports xs ys = fixAll $ map (\all@(ScalaImport root1 cls) -> foldl' merge' all (xs ++ ys)) (xs ++ ys)
    where

        merge' :: ScalaImport -> ScalaImport -> ScalaImport
        merge' acc sc = if acc == sc then acc { getModulesImport = getModulesImport acc ++ getModulesImport sc }
                                        else acc


        fixAll = map mergeAll . map (\im -> im { getModulesImport = nub (getModulesImport im) }) . nub



        mergeAll :: ScalaImport -> ScalaImport
        mergeAll im = case find (=="_") (getModulesImport im) of
                        Nothing -> checkSize im
                        Just _ -> im { getModulesImport = ["_"] }

        checkSize :: ScalaImport -> ScalaImport
        checkSize im = if length (getModulesImport im) > 3 
                            then im { getModulesImport = ["_"] }
                            else im

