{-# LANGUAGE OverloadedStrings #-}

module Cache where

import System.Directory
import Data.List (isSuffixOf)
import qualified Data.ByteString.Char8 as B
import Data.Maybe
import Control.Monad 
import qualified Data.HashTable.IO as H

import Types
import CreateImport


-- Un cache es una tabla has donde el primer elemento (key) es el nombre de la
-- clase y el segundo (val) es el directorio en el cual se encuentra
type Cache = H.BasicHashTable String String 



createCache :: String -> Int -> IO Cache
createCache androidFolder api = do
    let sources = androidFolder ++ "/sources/android-" ++ show api

    archivos <- liftM (filter soloJava) . todosArchivos $ sources

    vals <- liftM concat . mapM (extractClass sources) $ archivos

    insertar vals

    where
        soloJava :: String -> Bool
        soloJava = isSuffixOf ".java"


extractClass :: String -> String -> IO ([(String, String)])
extractClass sources file = do
    contents <- liftM (B.splitWith (==' ')) . B.readFile $ file


    let clases = map fromJust . filter isJust . map check . zip contents $ (tail contents)

    return . map (\c -> (B.unpack c, importName)) $ clases


    where
        check :: (B.ByteString, B.ByteString) -> Maybe B.ByteString
        check ("class", val) = Just val
        check _              = Nothing

        importName :: String
        importName = drop (length sources + 1) file 


todosArchivos :: String -> IO ([String])
todosArchivos dir = do
    esDirectorio <- doesDirectoryExist dir

    if esDirectorio then do
        files <- liftM (map (\f -> dir ++ "/" ++ f)) . liftM (filter ignoreDir) . getDirectoryContents $ dir
        enCarpeta <- liftM concat . mapM todosArchivos $ files

        return (files ++ enCarpeta)

    else return []

    where
        ignoreDir :: String -> Bool
        ignoreDir ".."              = False
        ignoreDir "."               = False
        ignoreDir "mock_android"    = False
        ignoreDir _                 = True



insertar :: [(String, String)] -> IO Cache
insertar list = do
    cache <- H.new :: IO Cache


    foldM insertar' cache list


    where
        insertar' :: Cache -> (String, String) -> IO Cache
        insertar' cache (key, val) = do
            maybe <- H.lookup cache key

            case maybe of
                Nothing -> H.insert cache key val
                Just x  -> if length x < length val
                                then return ()
                                else H.insert cache key val
            return cache
