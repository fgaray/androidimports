module Types where

import Data.ByteString.Char8 (ByteString)

-- ScalaImport Root Clases
data ScalaImport = ScalaImport { getRootImport    :: ByteString
                               , getModulesImport :: [ByteString]
                               }



data Command = Command { todo    :: Bool
                       , archivo :: Maybe String
                       } deriving Show
