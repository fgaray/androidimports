{-# LANGUAGE OverloadedStrings #-}
module CreateImport where

import qualified Data.ByteString.Char8 as B
import Data.ByteString.Char8 (ByteString)

import Data.Monoid
import Data.List


class (Monoid a, Show a) => LanguageImport a where
    createImport :: String -> a
    parseImport :: B.ByteString -> [a]











-- Replace the character o with the character c in the given string
replace :: String -> Char -> Char -> String
replace [] _ _      = []
replace (x:xs) o c  = (if x == o then c else x) : replace xs o c


replaceBS :: (Char -> (Bool, Char)) -> ByteString -> ByteString
replaceBS _ "" = ""
replaceBS f bs = if fst res then snd res `B.cons` replaceBS f (B.tail bs) else (B.head bs) `B.cons` replaceBS f (B.tail bs)
    where
        res = f (B.head bs)



-- Return a common root in the list
commonRoot :: Eq a => [a] -> [a] -> [a]
commonRoot xs ys = fst . unzip . takeWhile (\(x, y) -> x == y) $ zip xs ys




splitWith :: (Char -> Bool) -> String -> [String]
splitWith _ [] = []
splitWith f xs = takeWhile (not . f) xs : splitWith f (drop 1 . dropWhile (not . f) $ xs)


{-remove :: Eq a => (a -> Bool) -> [a] -> [a]-}
{-remove _ []     = []-}
{-remove f (x:xs) = if f x then remove f xs else x : remove f xs-}



concatWith :: [[a]] -> a -> [a]
concatWith [] _     = []
concatWith (x:xs) y = x ++ [y] ++ concatWith xs y


concatWithBS :: ByteString -> [ByteString] -> ByteString
concatWithBS _ []     = ""
concatWithBS s (x:xs) = x `B.append` s `B.append` concatWithBS s xs



