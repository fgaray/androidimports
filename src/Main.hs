{-# LANGUAGE OverloadedStrings #-}
module Main where


import System.Environment (getEnv)
import System.Directory
import Control.Monad (liftM)
import qualified Data.HashTable.IO as H
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as S
import Options.Applicative
import Data.Monoid
import Data.List (isSuffixOf)
import System.IO


import Cache
import FileWriter
import ParseArguments
import ScalaImports
import Types




cacheFile :: FilePath
cacheFile = "android_import.cache"

ignoreFile :: FilePath
ignoreFile = "ignore_android.txt"


main :: IO ()
main = do
    
    {-command <- execParser (info commandLine commandLine)-}

    {-print command-}

    



    androidFolder <- getEnv "ANDROID_HOME"

    parseArguments

    existe <- doesFileExist cacheFile
    ignoreFileExiste <- doesFileExist ignoreFile

    ignore <- if ignoreFileExiste
                then do
                    contents <- readFile ignoreFile
                    return $ S.fromList (read contents :: [ByteString]) `S.union` defaultIgnore
                else do
                    putStrLn $ "WARNING: file " ++ ignoreFile ++ " does not exist"
                    return $ defaultIgnore

    cache <- if not existe 
                then do
                    putStrLn "Creating a cache..."
                    cache <- createCache androidFolder 14
                    str <- liftM show . H.toList $ cache
                    writeFile cacheFile str
                    return cache

                else do
                    putStrLn "Opening cache..."
                    list <- (readFile cacheFile >>= (\s -> length s `seq` return s))
                    H.fromList (read list :: [(String, String)])

    files <- liftM (filter (isSuffixOf ".scala")) $ getDirectoryContents "."


    mapM_ (checkFile ignore cache) files
    

    return ()



checkFile :: S.Set ByteString -> Cache -> FilePath -> IO ()
checkFile ignore cache file = do
    putStrLn  file
    
    browser <- B.readFile file

    let mods   = obtenerModulos $ browser
        idents = getIdentificadores ignore $ browser


    imports <- generateImports cache $ notImported mods idents
    
    fileWriter file (mods `mergeImports` imports)



commandLine :: Parser Command
commandLine = Command
           <$> switch
                (  short 'a'
                <> long "all"
                <> help "If we should fix the imports of all the files "
                )
           <*> optional (strOption
                (  long "file"
                <> short 'f'
                <> metavar "FILE"
                <> help "The file to check if it need an aditional import"
                ))
