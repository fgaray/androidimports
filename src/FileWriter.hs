{-# LANGUAGE OverloadedStrings #-}
module FileWriter where

import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import System.IO (stdout)
import Data.List (sort)


import Types
import ScalaImports


fileWriter :: FilePath -> [ScalaImport] -> IO ()
fileWriter archivo imps = do
    contents <- B.readFile archivo

    let (before, after) = splitFile $ contents

    let imports = B.unlines . map B.pack . map show . sort $ imps

    B.writeFile archivo (before `B.append` imports `B.append` after)




splitFile :: ByteString -> (ByteString, ByteString)
splitFile bs = (B.unlines before, B.unlines after)
    where
        before = filter (\x -> not ("_root_" `B.isPrefixOf` ((B.splitWith (==' ') x) !! 1))) . isImportAndPackage . validos . B.lines $ bs

        after = checkNoImportPackage . B.lines $ bs

        isImportAndPackage = filter (\x -> (B.splitWith (==' ') x) !! 0 == "import" || (B.splitWith (==' ') x) !! 0 == "package")

        validos = filter (\x -> length (B.splitWith (==' ') x) > 1)

        checkNoImportPackage = filter (\x -> if length (B.splitWith (==' ') x) > 0 then (B.splitWith (==' ') x) !! 0 /= "import" && (B.splitWith (==' ') x) !! 0 /= "package" else True)
