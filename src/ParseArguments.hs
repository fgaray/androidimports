module ParseArguments where


import System.Console.GetOpt
import System.Environment (getArgs)
import System.Directory (doesFileExist)
import Control.Monad (liftM)
import Data.List (all)




data Flag = Version
          | Help
          | Files String
          | Api Int
          deriving Show



flags :: [OptDescr Flag]
flags = [ Option ['v'] ["version"] (NoArg Version) "show the current version"
        , Option ['h'] ["ayuda", "help"] (NoArg Help) "show this help"
        , Option ['f'] ["files"] (ReqArg Files "FILES") "the files to be checked if need an import"
        ]



parseArguments :: IO ([Flag])
parseArguments = do
        argv <- getArgs
        (flags, st) <- getOptions argv

        return flags



getOptions :: [String] -> IO ([Flag], [String])
getOptions argv = case getOpt Permute flags argv of
                        (o, n, []) -> do
                                return (o, n)
                        (_, _, err) -> ioError (userError ("Error: Argumentos no válidos"))
